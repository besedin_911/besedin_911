import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Programmer on 25.11.2015.
 */
public class Processing {

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "root");
    }

    public static ArrayList<Country> countries = new ArrayList<>();
    public static ArrayList<Cat> categories = new ArrayList<>();
    public static ArrayList<Product> products = new ArrayList<>();


    public static void categoriesList() throws SQLException, ClassNotFoundException {
        categories.clear();
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT id, parentid, category FROM category");
             ResultSet resultSet = ps.executeQuery();) {
             while (resultSet.next()) {
                int id = resultSet.getInt(1);
                int parentid = resultSet.getInt(2);
                String category = resultSet.getString(3);
                categories.add(new Cat(id, parentid, category));
             }
        }
    }

    public static void countriesList() throws SQLException, ClassNotFoundException {
        countries.clear();
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT id_country, name_country FROM hte_country");
             ResultSet resultSet = ps.executeQuery();) {
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String country = resultSet.getString(2);
                countries.add(new Country(id, country));
            }
        }
    }

    public static void productsList() throws SQLException, ClassNotFoundException {
        products.clear();
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("SELECT product, country, weight, category FROM product");
             ResultSet resultSet = ps.executeQuery();) {
            while (resultSet.next()) {
                String product = resultSet.getString(1);
                String country = resultSet.getString(2);
                String weight = resultSet.getString(3);
                String category = resultSet.getString(4);
                products.add(new Product(product, country, weight, category));
            }
        }
    }

//    public static final Pattern pattern1 = Pattern.compile
//            ("[0-9]");
//
//    public static boolean isCheck(String weight) {
//        Matcher matcher = pattern1.matcher(weight);
//        if (matcher.matches())
//            return true;
//        else return false;
//    }

    public static ArrayList<Country> getCountries() {
        return countries;
    }
    public static ArrayList<Cat> getCategories() {
        return categories;
    }
    public static ArrayList<Product> getProducts() {
        return products;
    }

    public static void addProduct(String text, String item, String text1, String item2) throws SQLException, ClassNotFoundException {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO product(product, country, weight, category)  VALUES(?,?,?,?)");
            preparedStatement.setString(1, text);
            preparedStatement.setString(2, item);
            preparedStatement.setString(3, text1);
            preparedStatement.setString(4, item2);
            preparedStatement.execute();
    }

    public static void addCategories(String text, int l) throws SQLException, ClassNotFoundException {
        Driver driver = new FabricMySQLDriver();
        DriverManager.registerDriver(driver);
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO category VALUES(?,?,?)");
        preparedStatement.setInt(1, categories.size());
        preparedStatement.setInt(2, l);
        preparedStatement.setString(3, text);
        preparedStatement.execute();
    }


}
