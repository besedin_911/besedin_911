import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.sql.SQLException;
import java.util.*;
import java.util.List;

public class Show extends javax.swing.JFrame {

        Show() {
            super();
            setResizable(false);
        }

    void initComponents() throws SQLException, ClassNotFoundException {

        Processing.productsList();
        TableModel model = new MyTableModel(Processing.getProducts());
        JTable table = new JTable(model);

        getContentPane().add(new JScrollPane(table));

        setPreferredSize(new Dimension(260, 220));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    public class MyTableModel implements TableModel {

        private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

        private List<Product> products;

        public MyTableModel(List<Product> products) {
            this.products = products;
        }

        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }

        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        public int getColumnCount() {
            return 4;
        }

        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "Продукт";
                case 1:
                    return "Страна";
                case 2:
                    return "Вес";
                case 3:
                    return "Категория";
            }
            return "";
        }

        public int getRowCount() {
            return products.size();
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Product product = products.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return product.getProduct();
                case 1:
                    return product.getCountry();
                case 2:
                    return product.getWeight();
                case 3:
                    return product.getCategory();
            }
            return "";
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }

        public void setValueAt(Object value, int rowIndex, int columnIndex) {

        }

    }
}
