/**
 * Created by Programmer on 05.12.2015.
 */
public class Cat {
    int id;
    int parentid;
    String category;

    Cat(int id, int parentid, String category) {
        this.id = id;
        this.parentid = parentid;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentid() {
        return parentid;
    }

    public void setParentid(int parentid) {
        this.parentid = parentid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
