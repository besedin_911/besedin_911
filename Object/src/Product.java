/**
 * Created by Programmer on 21.11.2015.
 */
public class Product {
    String product;
    String weight;
    String country;
    String category;

    Product(String product, String country, String weight, String category) {
        this.product = product;
        this.weight = weight;
        this.country = country;
        this.category = category;
    }

    public String getCountry() {
        return country;
    }

    public String getProduct() {
        return product;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWeight() {
        return weight;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
