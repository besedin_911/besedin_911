/**
 * Created by Programmer on 23.11.2015.
 */
public class Country {
    int id;
    String country;

    Country(int id, String country) {

        this.id = id;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
