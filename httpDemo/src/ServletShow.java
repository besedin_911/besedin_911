import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Programmer on 05.10.2015.
 */
@WebServlet(name = "ServletShow", urlPatterns = "/show")
public class ServletShow extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO.user();
        request.setAttribute("read", DAO.getRead());
        request.getRequestDispatcher("WEB-INF/show.jsp").forward(request, response);
    }
}


